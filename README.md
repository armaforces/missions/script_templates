Paczka modularnych skryptów ułatwiających tworzenie misji, aktualnie zawiera:
- Framework zadań

By dołączyć wybrany moduł kopiujemy jego folder do folderu `scriptModules` w misji i dodajemy odpowiednią linijkę z pliku `modules.hpp` do takiego samego pliku w folderze `scriptModules` w misji. Najlepiej kopiować tylko te moduły, z których chcemy korzystać, żeby uniknąć problemów związanych z automatycznym uruchamianiem się przykładów zawartych w innych modułach.