#include "..\macros.hpp"

/*
	AF_Tasks_fnc_initTaskConditions

	Description:
		Funkcja inicjalizuje warunki zakończenia zadań.

	Parameter(s):
		NOTHING

	Returns:
		NOTHING
*/

if (!isServer) exitWith {};

/*
// Tworzy skrypt oczekujący na spełnienie warunków
[{<warunek>}, {
	<skrypt>
}, [<dodatkowe parametry>], <czas oczekiwania na spełnienie warunków>] call CBA_fnc_waitUntilAndExecute;
// <warunek> - musi zwrócic true/false
// <skrypt> - zostanie wykonany, gdy warunek będzie spełniony
// <dodatkowe parametry> - mogą byc używane przez skrypt
// <czas oczekiwania na spełnienie warunków> - po tym czasie spełnienie warunków uruchomienia skryptu nic nie da, -1 dla braku ograniczenia
*/

// Ludzie umarli
// Zadanie przegrane gdy wszyscy wyznaczeni ludzie zginą
// Skrypt warunkowy dla każdego człowieka do uratowania
Rescue_Guys = [rescue_man_1, rescue_man_2, rescue_man_3];
[{!alive rescue_man_1}, {
	Rescue_Guys = Rescue_Guys - [rescue_man_1];
}, [], -1] call CBA_fnc_waitUntilAndExecute;
[{!alive rescue_man_2}, {
	Rescue_Guys = Rescue_Guys - [rescue_man_2];
}, [], -1] call CBA_fnc_waitUntilAndExecute;
[{!alive rescue_man_3}, {
	Rescue_Guys = Rescue_Guys - [rescue_man_3];
}, [], -1] call CBA_fnc_waitUntilAndExecute;
[{(count Rescue_Guys == 0)}, {
	// Sprawdzenie czy goście nie zostali już uratowani
	if (isNil 'guys_rescued') exitWith {
		[t_main, false] call AF_Tasks_fnc_setTaskSuccess;
		guys_rescued = false;
	};
}, [], -1] call CBA_fnc_waitUntilAndExecute;

/*
AF_Tasks_fnc_inArea
Sprawdza czy wszystkie jednostki z grupy znajdują się w strefie wyznaczonej marker i promień od niego.
Jeśli chcemy sprawdzić w obszarze markera (gdy mamy marker obszarowy) to pomijamy ostatni parametr.

[<grupa jednostek do sprawdzenia>, <marker_centrum_strefy>, (<promien_strefy>)] call AF_Tasks_fnc_inArea;

Jeśli chcemy sprawdzić tylko jedną jednostkę to dajemy ją np. zeus,
ale już dla jednostek z grupy zeusa byłoby: units group zeus
*/
// Ludzie uratowani
// Zadanie wykonane jeśli wszyscy żywi spośród wyznaczonych ludzi znajdą się w odpowiedniej strefie
[{[Rescue_Guys, 'sys_base', 15] call AF_Tasks_fnc_inArea}, {
	// Warunek [Rescue_Guys, 'sys_base', 15] call AF_Tasks_fnc_inArea zwróci true jeśli Rescue_Guys będzie pustą tablicą, więc sprawdzamy czy przypadkiem tak nie jest
	if (count Rescue_Guys == 0) exitWith {};
	[t_main, true] call AF_Tasks_fnc_setTaskSuccess;
	// Alternatywnie (tak samo zadziała dla zaliczenia zadania) można użyć:
	// [t_main] call AF_Tasks_fnc_setTaskSuccess;
	guys_rescued = true;
}, [], -1] call CBA_fnc_waitUntilAndExecute;

/*
AF_Tasks_fnc_areaClear
Sprawdza czy strefa jest wolna od jednostek danej strony

[<strona do sprawdzenia>, <marker_centrum_strefy>, (<promien_strefy>)] call AF_Tasks_fnc_areaClear;
*/
// Jednostki strony wschodniej wyeliminowane w promieniu 500 m od markeru flagi
[{['EAST', 'marker_flag', 500] call AF_Tasks_fnc_areaClear}, {
	[t_main, true] call AF_Tasks_fnc_setTaskSuccess;
}, [], -1] call CBA_fnc_waitUntilAndExecute;


/*
AF_Tasks_fnc_anyoneInArea
Sprawdza czy którakolwiek jednostka znajduje się w strefie. Działanie podobne do AF_Tasks_fnc_inArea

[<grupa jednostek do sprawdzenia>, <marker_centrum_strefy>, (<promien_strefy>)] call AF_Tasks_fnc_anyoneInArea;
*/
// Dowolny gracz wszedł w strefę o promieniu 500 m od markera 'sys_target_area'

[{[allPlayers, 'sys_target_area', 500] call AF_Tasks_fnc_anyoneInArea}, {
	[t_main, true] call AF_Tasks_fnc_setTaskSuccess;
}, [], -1] call CBA_fnc_waitUntilAndExecute;


// Intel przeczytany
// Polecenie 'remoteExec' wykonuje skrypt na innych komputerach (w tym przypadku na wszystkich, bo ma drugi parametr = 0. Gdyby był = 2 to oznacza wykonanie tylko na serwerze).
[intel_object, "Przeczytaj intel", "", "", "true", "true", {}, {}, {
	private _msg = "Tekst intelu";
	// Wyświetl tekst
	titleText [_msg, "PLAIN DOWN", 1];
	// Zalicz zadanie na serwerze (akcje dodane przez 'BIS_fnc_holdActionAdd' i 'addAction' są lokalne u każdego gracza, więc trzeba wysłać polecenie na serwer)
	[t_main, true] remoteExec ["AF_Tasks_fnc_setTaskSuccess", 2];
}, {}, [], 2, 10, false, false] remoteExec ["BIS_fnc_holdActionAdd", 0, true];