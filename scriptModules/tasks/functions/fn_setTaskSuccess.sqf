#include "..\macros.hpp"

/*
	AF_Tasks_fnc_setTaskSuccess

	Description:
		Funkcja zmienia status zadania na zaliczone lub niezaliczone.

	Parameter(s):
		_task - zmienna wskazująca zadanie w bibliotece zadań [ARRAY]
		_state - stan zadania zaliczone/niezaliczone (true/false) [BOOLEAN]

	Returns:
		NOTHING
*/

params ["_task", ["_state", true]];

[GET_TASK_ID(_task), ["FAILED", "SUCCEEDED"] select _state] call BIS_fnc_taskSetState;