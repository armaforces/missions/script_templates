#include "..\macros.hpp"

/*
	AF_Tasks_fnc_areaClear

	Description:
		Function check if there are any units belonging to given side are in marker's area or in desired radius.

	Parameter(s):
		0: Side [SIDE/STRING]
        1: Marker [STRING]
        2: Radius [NUMBER] (optional)

	Returns:
		Result [BOOL]
*/

params ["_enemySide", "_markerName", ["_radius", 0]];

if (_radius >= 0) then {
    ((allUnits inAreaArray [getMarkerPos _markerName, _radius, _radius]) findIf {side _x isEqualTo _enemySide}) isEqualTo -1;
} else {
    ((allUnits inAreaArray _markerName) findIf {side _x isEqualTo _enemySide}) isEqualTo -1;
};

