#include "..\macros.hpp"

/*
	AF_Tasks_fnc_createTask

	Description:
		Funkcja tworzy zadanie na podstawie biblioteki zadań.

	Parameter(s):
		_task - zmienna wskazująca zadanie w bibliotece zadań [ARRAY]
		_taskParent - zmienna wskazująca zadanie nadrzędne (to pod którym zostanie utworzone zadanie _task) w bibliotece zadań [ARRAY]
		_taskGroup - zmienna wskazująca komu dodać zadanie [ARRAY]
		_createdState - startowy status zadania [STRING] // Statusy zadań: "CREATED", "ASSIGNED", "SUCCEEDED", "FAILED", "CANCELED"

	Returns:
		NOTHING
*/

params ["_task", ["_taskParent", []], ["_taskGroup", TaskGroup_All], ["_createdState", "CREATED"]];

if (_taskParent isEqualTo []) then {
	CREATE_MAIN_TASK(_taskGroup, _task, _createdState);
} else {
	CREATE_TASK(_taskGroup, _taskParent, _task, _createdState);
};