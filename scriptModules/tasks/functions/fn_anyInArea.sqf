#include "..\macros.hpp"

/*
	AF_Tasks_fnc_anyoneInArea

	Description:
		Function check if any unit is in marker's area or in desired radius.

	Parameter(s):
		0: Units [ARRAY]
        1: Marker [STRING]
        2: Radius [NUMBER] (optional)

	Returns:
		Result [BOOL]
*/

params ["_units", "_markerName", ["_radius", 0]];

if (_units isEqualType objNull) then {_units = [_units]};

if (_radius >= 0) then {
    !((_units inAreaArray [getMarkerPos _markerName, _radius, _radius]) isEqualTo [])
} else {
    !((_units inAreaArray _markerName) isEqualTo [])
};
