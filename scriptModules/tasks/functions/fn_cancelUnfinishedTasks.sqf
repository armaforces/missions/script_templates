/*
	AF_Tasks_fnc_cancelUnfinishedTasks

	Description:
		Funkcja anuluje wszystkie nieukończone zadania.

	Parameter(s):
		NOTHING

	Returns:
		NOTHING
*/

{
	if !(_x call BIS_fnc_taskCompleted) then {
		[_x, "CANCELED"] call BIS_fnc_taskSetState;
	};
} forEach (zeus call BIS_fnc_tasksUnit);