#include "..\macros.hpp"

/*
	AF_Tasks_fnc_initTasks

	Description:
		Funkcja inicjalizuje zadania.

	Parameter(s):
		NOTHING

	Returns:
		NOTHING
*/


if (!isServer) exitwith {};

// Ustalenie grup, którym zostaną przydzielone zadania
// Zeus (i jego padawan dzięki 'units group') znajduje się w każdej grupie,
// której są przydzielane zadania i również widzi jak idzie graczom
TaskGroup_Blufor = [west, units group zeus];
TaskGroup_Redfor = [east, units group zeus];
TaskGroup_Indfor = [independent, units group zeus];
TaskGroup_All = [west, east, independent, units group zeus];
// Można też umieścić konkretne jednostki lub grupy (pamiętać o dodaniu 'units group zeus' jak wyżej!):
//	TaskGroup_CustomGroup = [<group1>, <player1>, units group <player2>]

// Statusy zadań:
//  "CREATED"
//  "ASSIGNED"
//  "SUCCEEDED"
//  "FAILED"
//  "CANCELED"

/*
	AF_Tasks_fnc_createTask

	Parametry:
	0: zmienna zadania do stworzenia
	Opcjonalne:
	1: zmienna zadania nadrzędnego. Domyślnie: []
	2: zmienna grupy do której będzie przypisane zadanie. Domyślnie: TaskGroup_All
	3: status tworzonego zadania. Domyślnie: "CREATED"
*/

// Tworzy zadanie główne
[t_main] call AF_Tasks_fnc_createTask;
// Tworzy zadanie podrzędne
[t_template, t_main] call AF_Tasks_fnc_createTask;

// Tworzy zadanie główne dla strony czerwonej
[t_main, [], TaskGroup_Redfor] call AF_Tasks_fnc_createTask;
// Tworzy zadanie podrzędne dla strony czerwonej, które zostanie im przypisane (będzie miało wypełniony kwadracik, wskazuje na to, że należy wykonywać to zadanie)
[t_template, t_main, TaskGroup_Redfor, "ASSIGNED"] call AF_Tasks_fnc_createTask;

// UWAGA!!!
// Zadanie podrzędne musi być przypisane do tej samej grupy co nadrzędne,
// inaczej zadanie nadrzędne dla tego zadania będzie puste.

// UWAGA 2
// W podanym przykładzie jednostki strony czerwonej będą miały 4 zadania, a jednostki pozostałych stron tylko 2.
