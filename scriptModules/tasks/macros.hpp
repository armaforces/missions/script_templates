// Task creation macros
#define GET_TASK_ID(TASK)									(TASK select 0)
#define GET_TASK_TITLE(TASK)								(TASK select 1)
#define GET_TASK_DESCRIPTION(TASK)							(TASK select 2)
#define GET_TASK_ICON(TASK)									(TASK select 3)
#define GET_TASK_MARKER(TASK)  								(TASK select 4)
#define GET_TASK_MARKER_POS(TASK)							if (GET_TASK_MARKER(TASK)!="") then { getMarkerPos GET_TASK_MARKER(TASK) } else { objNull }
#define GET_TASK_POS(TASK)									if (GET_TASK_MARKER(TASK) isEqualType "") then { GET_TASK_MARKER_POS(TASK) } else { if (GET_TASK_MARKER(TASK) isEqualType objNull) then { GET_TASK_MARKER(TASK) } else { objNull } }
#define GET_TASK_DESCRIPTION_ARRAY(TASK) 					[GET_TASK_DESCRIPTION(TASK), GET_TASK_TITLE(TASK), GET_TASK_MARKER(TASK)]
#define CREATE_MAIN_TASK(TARGET, TASK, STATUS)				[TARGET, GET_TASK_ID(TASK), GET_TASK_DESCRIPTION_ARRAY(TASK), GET_TASK_MARKER_POS(TASK), STATUS, 10, false, GET_TASK_ICON(TASK)] call BIS_fnc_taskCreate
#define CREATE_TASK(TARGET, MAIN_TASK, TASK, STATUS)		[TARGET, [GET_TASK_ID(TASK), GET_TASK_ID(MAIN_TASK)], GET_TASK_DESCRIPTION_ARRAY(TASK), GET_TASK_POS(TASK), STATUS, 10, false, GET_TASK_ICON(TASK)] call BIS_fnc_taskCreate

// Task library macros
#define MARKER_LINK(MARKER_NAME, LINK_TEXT)					+ "<marker name='"+MARKER_NAME+"'>"+LINK_TEXT+"</marker>" +
