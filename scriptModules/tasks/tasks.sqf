#include "macros.hpp"

// Biblioteka zadań
// Tutaj znajdują się wszystkie zadania, które będą wykorzystywane w innych skryptach modułu zadań
// Ikonki zadań do wzięcia z: https://community.bistudio.com/wiki/Arma_3_Tasks_Overhaul#Appendix

t_main = [
	't_main', // ID zadania
	"GENERIC MISSION TITLE", // Tytuł zadania
	"GENERIC MISSION DESCRIPTION", // Opis misji
	"", // Ikona zadania
	"" // Znacznik miejsca zadania (tj. miejsce, w którym będzie ikonka zadania). Jeśli puste to ikonka zadania nie będzie pokazana na mapie
];

t_template = [
	't_template', // ID zadania
	"Template zadania", // Tytuł zadania
	"Template opisu misji w miejscu "MARKER_LINK('respawn','znacznika respawn')", tworzonego po starcie gry.", // Opis misji
	"attack", // Ikona zadania
	"respawn" // Znacznik miejsca zadania (tj. miejsce, w którym będzie ikonka zadania). Jeśli puste to ikonka zadania nie będzie pokazana na mapie
];