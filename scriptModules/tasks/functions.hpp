class AF_Tasks {
	class tasks {
			file = "scriptModules\tasks\functions";
			// Initialize tasks
			class initTasks {
				postInit = 1;
			};
			class initTaskLibrary {
				preInit = 1;
			};
			class initTaskConditions {
				postInit = 1;
			};

            // Conditions check
            class allInArea {}; // Returns true when every unit is in area
            class anyInArea {}; // Returns true when any unit is in area
            class areaClear {}; // Returns true when there are no units from desired side in area

            // Utility
			class cancelUnfinishedTasks {};
            class createTask {};
			class setTaskSuccess {};
	};
};