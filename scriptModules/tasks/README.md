**Framework zadań**

W pliku `tasks.sqf` znajduje się biblioteka zadań, która jest wykorzystywana w pozostałych plikach do m.in. inicjalizacji zadań.

Samo tworzenie zadań odbywa się w pliku `fn_initTasks.sqf`, a tworzenie warunków wykonania zadań w pliku `fn_initTaskConditions.sqf`. Jeśli nie interesują nas warunki wykonania zadań to plik ten można WYCZYŚCIĆ (nie usunąć!).

Przykładowe zadania zostały utworzone i powinny stanowić wzór do kopiowania i wypełniania.
Nie przenosić nic pomiędzy plikami (np. nie dodawać nic do biblioteki poza kolejnymi zadaniami), tak jak jest będzie działać.
Wszystkie funkcje zadań utworzone w ramach tego modułu używają zmiennej zadania z biblioteki do działania.