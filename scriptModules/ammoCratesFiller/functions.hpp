class AF_ACF {
    class ammoCratesFiller {
            file = "scriptModules\ammoCratesFiller\functions";
            // Fill base crates
            class fillCrates {
                postInit = 1;
            };
            class fillEquipment {};
            class fillThrowables {};
            class configCheck {
                preInit = 1;
            };
    };
};