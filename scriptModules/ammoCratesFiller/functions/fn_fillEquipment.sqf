/*
    AF_ACF_fnc_fillEquipment

    Description:
        Funkcja wypełnia skrzynię z ekwipunkiem specjalnym.


    Parameter(s):
        _crate - skrzynia do wypełnienia <OBJECT>

    Returns:
        NOTHING
*/

#define ACE_SPRAYS ["ACE_SpraypaintBlack", "ACE_SpraypaintRed", "ACE_SpraypaintBlue", "ACE_SpraypaintGreen"]
//#define ACE_EXPLOSIVES ["ACE_Clacker", "DemoCharge_Remote_Mag", "SatchelCharge_Remote_Mag"]
#define ACRE_RADIOS ["ACRE_PRC117F", "ACRE_PRC152", "ACRE_PRC343"]
#define ACRE_RADIOS_COUNT [10, 30, 30]

params ["_crate"];

private _fnc_toCrate = {
	params ["_crate", "_Items", ["_quantity", 1]];
    if (_Items isEqualType []) then {
        {
            _crate addItemCargoGlobal [_x, _quantity];
        } forEach _Items;
    };
    if (_Items isEqualType "") then {
        _crate addItemCargoGlobal [_Items, _quantity];
    };
};

[_crate, ACE_SPRAYS, 10] call _fnc_toCrate;
//[_crate, ACE_EXPLOSIVES, 10] call _fnc_toCrate;
{[_crate, _x, ACRE_RADIOS_COUNT select _forEachIndex] call _fnc_toCrate;} forEach ACRE_RADIOS;
