/*
	AF_ACF_fnc_fillThrowables

	Description:
		Funkcja wypełnia skrzynię z granatami i innymi przedmiotami rzucanymi


	Parameter(s):
		_crate - skrzynia do wypełnienia <OBJECT>

	Returns:
		NOTHING
*/

#define ACE_HAND_FLARES ["ACE_HandFlare_Yellow", "ACE_HandFlare_White", "ACE_HandFlare_Red", "ACE_HandFlare_Green"]
#define ACE_CHEMLIGHTS ["ACE_Chemlight_White", "ACE_Chemlight_HiYellow", "ACE_Chemlight_HiWhite", "ACE_Chemlight_HiRed", "ACE_Chemlight_HiBlue", "ACE_Chemlight_HiGreen", "ACE_Chemlight_IR", "ACE_Chemlight_Orange", "ACE_Chemlight_UltraHiOrange"]
#define CHEMLIGHTS ["Chemlight_yellow", "Chemlight_red", "Chemlight_blue", "Chemlight_green"]
#define HE_GRENADES ["rhs_mag_m67", "rhs_mag_mk3a2"]
#define SMOKE_GRENADES ["SmokeShellYellow", "SmokeShellRed", "SmokeShellPurple", "SmokeShellBlue", "SmokeShellOrange", "SmokeShellGreen", "SmokeShell"]

params ["_crate"];

private _fnc_toCrate = {
	params ["_crate", "_Items", ["_quantity", 1]];
    if (_Items isEqualType []) then {
        {
            _crate addItemCargoGlobal [_x, _quantity];
        } forEach _Items;
    };
    if (_Items isEqualType "") then {
        _crate addItemCargoGlobal [_Items, _quantity];
    };
};

[_crate, ACE_HAND_FLARES, 10] call _fnc_toCrate;
[_crate, ACE_CHEMLIGHTS, 10] call _fnc_toCrate;
[_crate, CHEMLIGHTS, 10] call _fnc_toCrate;
[_crate, HE_GRENADES, 10] call _fnc_toCrate;
[_crate, SMOKE_GRENADES, 10] call _fnc_toCrate;
