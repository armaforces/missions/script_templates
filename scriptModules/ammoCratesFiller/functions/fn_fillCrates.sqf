/*
    AF_ACF_fnc_fillCrates

    Description:
        Funkcja wypełnia startowe skrzynie w bazie sprzętem.

    Parameter(s):
        NOTHING

    Returns:
        NOTHING
*/

#define BASE_CRATE_TYPES ["B_CargoNet_01_ammo_F", "Box_NATO_Equip_F", "B_supplyCrate_F", "Box_NATO_WpsSpecial_F"]
#define CRATE_SEARCH_RADIUS 300

if (!isServer) exitWith {};

BaseCrates = (respawn_coop nearObjects ["ThingX", CRATE_SEARCH_RADIUS]) select {typeOf _x in BASE_CRATE_TYPES};

// ammo
// Equip
// supply
// Special

{
    if (["ammo", typeOf _x] call BIS_fnc_inString) then {
        // AK mags
        _x addItemCargoGlobal ["CUP_30Rnd_545x39_AK_M", 100];
        // GL rounds
        _x addItemCargoGlobal ["rhs_VOG25", 50];
        _x addItemCargoGlobal ["rhs_GRD40_White", 20];
        _x addItemCargoGlobal ["rhs_GRD40_Red", 20];
        _x addItemCargoGlobal ["rhs_GRD40_Green", 20];
        // SVD
        _x addItemCargoGlobal ["rhs_weap_svds", 2];
        _x addItemCargoGlobal ["rhs_acc_pso1m21", 2];
        _x addItemCargoGlobal ["rhs_acc_1pn93_1", 2];
        _x addItemCargoGlobal ["rhs_10Rnd_762x54mmR_7N1", 20];
        // MG mags
        _x addItemCargoGlobal ["CUP_45Rnd_TE4_LRT4_Green_Tracer_545x39_RPK_M", 30];
        _x addItemCargoGlobal ["CUP_60Rnd_545x39_AK74M_M", 30];
        // Pistol ammo
        _x addItemCargoGlobal ["rhs_mag_9x19_17", 30];
        // Igla
        _x addItemCargoGlobal ["rhs_weap_igla", 1];
        _x addItemCargoGlobal ["rhs_mag_9k38_rocket", 3];
        // RPG-7
        #define RPGMAGS_COUNT 6
        _x addItemCargoGlobal ["rhs_weap_rpg7", 2];
        _x addItemCargoGlobal ["rhs_acc_pgo7v3", 2];
        _x addItemCargoGlobal ["rhs_rpg7_OG7V_mag", RPGMAGS_COUNT];
        _x addItemCargoGlobal ["rhs_rpg7_PG7V_mag", RPGMAGS_COUNT];
        _x addItemCargoGlobal ["rhs_rpg7_PG7VL_mag", RPGMAGS_COUNT];
        _x addItemCargoGlobal ["rhs_rpg7_PG7VR_mag", RPGMAGS_COUNT];
        _x addItemCargoGlobal ["rhs_rpg7_TBG7V_mag", RPGMAGS_COUNT];
        _x addItemCargoGlobal ["rhs_rpg7_type69_airburst_mag", RPGMAGS_COUNT];
        // LAT
        _x addItemCargoGlobal ["rhs_weap_rpg26", 8];
        _x addItemCargoGlobal ["rhs_weap_rshg2", 16];
        // Satchels
        _x addItemCargoGlobal ["DemoCharge_Remote_Mag", 8];
        _x addItemCargoGlobal ["SatchelCharge_Remote_Mag", 4];
        _x addItemCargoGlobal ["ACE_M26_Clacker", 2];
    };
    if (["Equip", typeOf _x] call BIS_fnc_inString) then {
        // Add radios and sprays
        [_x] call AF_ACF_fnc_fillEquipment;
        // Backpacks
        _x addBackpackCargoGlobal ["rhs_sidor", 8];
        _x addBackpackCargoGlobal ["rhs_rpg_empty", 8];
        _x addBackpackCargoGlobal ["rhs_assault_umbts", 8];
        // NVG
        _x addItemCargoGlobal ["rhs_1PN138", 50];
        // Additional items
		_x addItemCargoGlobal ["ItemGPS", 16];
		_x addItemCargoGlobal ["B_UavTerminal", 2];
        _x addItemCargoGlobal ["ACE_DAGR", 8];
    };
    if (["Special", typeOf _x] call BIS_fnc_inString) then {
        // Add grenades, flares and chemlights
		[_x] call AF_ACF_fnc_fillThrowables;
        // Flashbangs
		_x addItemCargoGlobal ["ACE_M84", 50];
    };
} forEach BaseCrates;