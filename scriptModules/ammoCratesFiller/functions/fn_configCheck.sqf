/*
    AF_ACF_fnc_configCheck

    Description:
        Funkcja sprawdza dostępne addony

    Parameter(s):
        NOTHING

    Returns:
        NOTHING
*/

ACE_Loaded = isClass (configFile >> "CfgPatches" >> "ace_common");
RHS_USAF_Loaded = isClass (configFile >> "CfgPatches" >> "rhsusf_c_weapons");
RHS_AFRF_Loaded = isClass (configFile >> "CfgPatches" >> "rhs_c_weapons");