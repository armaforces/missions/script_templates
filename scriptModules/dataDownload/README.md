**Pobieranie danych**

Moduł dodaje skrypt na pobieranie danych z serwera.

Jedynym plikiem, który nas interesuje jest `functions\fn_initModule.sqf`, tam wprowadzamy wszystkie potrzebne rzeczy do uruchomienia skryptu.
Ustawiamy przede wszystkim `AF_DataDownload_Object`!
Jeśli nic więcej nie zmienimy to skrypt zadziała standardowo.
Tymczasowo czas trwania transferu (makra `TRANSFER_TIME`) nie ma żadnego efektu poza `TRANSFER_TIME_MAX`, który służy do sprawdzenia czy parametry wybrane przez użytkownika nie spowodują zbyt długiego czasu trwania pobierania.

Wycinek fn_initModule.sqf:
```
/*
	TU MOŻNA ZMIENIAĆ
*/

// Szybkość transferu (w jednostce pojemności)
#define TRANSFER_MAX		KILOBYTES(600)
#define TRANSFER_AVG		KILOBYTES(450)
#define TRANSFER_MIN		KILOBYTES(300)

// Czas trwania transferu (w jednostce czasu)
#define TRANSFER_TIME_MAX	MINUTES(5)
#define TRANSFER_TIME_AVG	MINUTES(4)
#define TRANSFER_TIME_MIN	MINUTES(3)

// Obiekt do którego przypisana będzie akcja
AF_DataDownload_Object = objNull;
// Postęp pobierania (na starcie 0)
AF_DataDownload_Progress = 0;
// Cel pobierania (w jednostce pojemności)
AF_DataDownload_Target = MEGABYTES(120);

/*
	TU JUŻ NIE
*/
```

Skrypt przed odpaleniem przeprowadzi uproszczony autotest i jeśli coś nie będzie działać poprawnie to wypluje odpowiednią informację i przerwie działanie (zadziała to również na serwerze).