/*
    AF_DataDownload_fnc_formatTime

    Description:
        Funkcja formatuje czas (w sekundach).

    Parameter(s):
        _time - czas (w sekundach) do sformatowania

    Returns:
        0: sformatowany czas [STRING]
*/

params ["_time"];

private _hrs = floor (_time/3600);
private _min = floor (_time/60);
private _sec = floor (_time - _min*60 - _hrs*3600);
/*if (_min < 10) then {
    _min = format ["0%1",_min];
};
if (_sec < 10) then {
    _sec = format ["0%1",_sec];
};*/

if (_hrs > 0) then {_hrs = str (_hrs) + " h "} else {""};
if (_min > 0) then {_min = str (_min) + " m "} else {""};
if (_sec > 0) then {_sec = str (_sec) + " s"} else {"0 s"};

format ["%1%2%3",_hrs,_min,_sec];
