/*
    AF_DataDownload_fnc_startDownload

    Description:
        Funkcja rozpoczyna proces pobierania danych.

    Parameter(s):
        _object - obiekt z którego ma rozpocząć się pobieranie

    Returns:
        NOTHING
*/

params ["_object"];

if (!isServer) exitWith {["AF_DataDownload_startDownload", [_object]] call CBA_fnc_serverEvent};

if (!isNil {_object getVariable ["AF_DataDownload_Started", nil]}) exitWith {};

_object setVariable ["AF_DataDownload_Started", true];

[_object] call AF_DataDownload_fnc_downloadLoop;
[_object] remoteExec ["AF_DataDownload_fnc_showProgress", 0];
["AF_DataDownload_AddHackAction", [_object]] call CBA_fnc_serverEvent;
