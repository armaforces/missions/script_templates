/*
    AF_DataDownload_fnc_initModule

    Description:
        Funkcja inicjalizuje moduł 'dataDownload'

    Parameter(s):
        NOTHING

    Returns:
        NOTHING
*/

if (!isServer) exitWith {};

#define QUOTE(X)	#X

#define GIGABYTES(NUMBER) 	NUMBER*1024*MEGABYTES(1)
#define MEGABYTES(NUMBER) 	NUMBER*1024*KILOBYTES(1)
#define KILOBYTES(NUMBER) 	NUMBER*1024*BYTES(1)
#define BYTES(NUMBER) 		NUMBER
#define BITS(NUMBER)		NUMBER*8

#define HOURS(NUMBER)		NUMBER*MINUTES(1)
#define MINUTES(NUMBER)		NUMBER*60*SECONDS(1)
#define SECONDS(NUMBER)		NUMBER

// Autotests and error handling
#define MORE_ERRORS			_errors = _errors + 1
#define ERROR_MSG(MSG)		MSG remoteExec ["systemChat", 0]

/*
    TU MOŻNA ZMIENIAĆ
*/

// Szybkość transferu (w jednostce pojemności)
#define TRANSFER_MAX		KILOBYTES(600)
#define TRANSFER_AVG		KILOBYTES(450)
#define TRANSFER_MIN		KILOBYTES(300)

// Czas trwania transferu (w jednostce czasu)
#define TRANSFER_TIME_MAX	MINUTES(5)
#define TRANSFER_TIME_AVG	MINUTES(4)
#define TRANSFER_TIME_MIN	MINUTES(3)

// Obiekty do których przypisana będzie akcja
AF_DataDownload_Objects = [
    laptok_1,
    laptok_2,
    laptok_3
];
// Postęp pobierania (na starcie 0)
AF_DataDownload_Progress = 0;
// Cel pobierania (w jednostce pojemności)
AF_DataDownload_Target = MEGABYTES(120);

/*
    TU JUŻ NIE
*/

// Pre-start Autotest
private _errors = 0;

// Obiekt został poprawnie zdefiniowany
if ((AF_DataDownload_Objects findIf {_x isEqualTo objNull} != -1) || !(AF_DataDownload_Objects findIf {_x isEqualType objNull} != -1)) then {MORE_ERRORS; ERROR_MSG("[AF dataDownload] Obiekt nie zdefiniowany lub nie istnieje!");};
// Przewidywany czas pobierania nie przekracza maksymalnego wyznaczonego (TRANSFER_TIME_MAX)
if ((AF_DataDownload_Target / (TRANSFER_AVG)) > TRANSFER_TIME_MAX) then {MORE_ERRORS; private _msg = format ["[AF dataDownload] Przewidywany czas pobierania (%1) jest dłuższy niż maksymalny wyznaczony (%2)!", (AF_DataDownload_Target/(TRANSFER_AVG)), TRANSFER_TIME_MAX]; ERROR_MSG(_msg);};

if (_errors > 0) exitWith {private _msg = format ["[AF dataDownload] Skrypt przerwany. Błędy: %1", _errors]; ERROR_MSG(_msg);};

// Magia skryptów

AF_DataDownload_Transfer = {
    (floor ((random [TRANSFER_MIN, TRANSFER_AVG, TRANSFER_MAX])*100))/100
};

["AF_DataDownload_startDownload", {
    params ["_object"];
    [_object] call AF_DataDownload_fnc_startDownload;
}] call CBA_fnc_addEventHandler;

{
    _x call AF_DataDownload_fnc_assignActions;
} forEach AF_DataDownload_Objects;

["AF_DataDownload_success", {
    params ["_object"];
    if (!isNil {_object getVariable ["AF_DataDownload_Success", nil]}) exitWith {};
    _object setVariable ["AF_DataDownload_Success", true, true];
    _object remoteExec ["removeAllActions", 0, true];
    // Akcje do podjęcia po udanym pobieraniu danych z obiektu:
    if (_object isEqualTo laptok_1) exitWith {/* Nic się tu nie dzieje, a mogłoby */};
    if (_object isEqualTo laptok_2) exitWith {/* Nic się tu nie dzieje, a mogłoby */};
    if (_object isEqualTo laptok_3) exitWith {/* Nic się tu nie dzieje, a mogłoby */};
}] call CBA_fnc_addEventHandler;

["AF_DataDownload_failure", {
    params ["_object"];
    if (!isNil {_object getVariable ["AF_DataDownload_Success", nil]}) exitWith {};
    _object setVariable ["AF_DataDownload_Success", false, true];
    _object remoteExec ["removeAllActions", 0, true];
    // Akcje do podjęcia po nieudanym pobieraniu danych z obiektu:
    if (_object isEqualTo laptok_1) exitWith {/* Nic się tu nie dzieje, a mogłoby */};
    if (_object isEqualTo laptok_2) exitWith {/* Nic się tu nie dzieje, a mogłoby */};
    if (_object isEqualTo laptok_3) exitWith {/* Nic się tu nie dzieje, a mogłoby */};
}] call CBA_fnc_addEventHandler;

["AF_DataDownload_Hacked", {
    params ["_object"];
    ["AF_DataDownload_failure", [_object]] call CBA_fnc_serverEvent;
}] call CBA_fnc_addEventHandler;
