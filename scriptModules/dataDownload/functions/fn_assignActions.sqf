/*
    AF_DataDownload_fnc_assignActions

    Description:
        Funkcja przypisuje do obiektu odpowiednie akcje umożliwiające rozpoczęcie pobierania.

    Parameter(s):
        _object - obiekt do przypisania akcji

    Returns:
        NOTHING
*/

params ["_object"];

[_object, "Rozpocznij transfer danych", "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa", "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa", "_this distance _target < 3", "_caller distance _target < 3", {
    // Code start
    params ["_target", "_caller", "_actionId", "_arguments"];
}, {
    // Code progress
    params ["_target", "_caller", "_actionId", "_arguments", "_progress", "_maxProgress"];
    private _msg = format ["Sprawdzanie penisa: %1%2", ((_progress/_maxProgress)*100) toFixed 1, "%"];
    titleText [_msg, "PLAIN DOWN", 0.05];
}, {
    // Code completed
    params ["_target", "_caller", "_actionId", "_arguments"];
    if (!isNil {_target getVariable ["AF_DataDownload_Success", nil]}) exitWith {
        [_target, _actionId] remoteExec ["BIS_fnc_holdActionRemove", 0, true];
    };
    if ("UMI_Dickhead" isEqualTo (headgear player)) then {
        titleText ["Witamy Panie Wielki Penis!", "PLAIN DOWN", 1];
        private _msg = format ["%1 ma wielkiego penisa!", name player];
        [_msg] remoteExec ["systemChat", 0];
        removeHeadgear player;
        [_target, _actionId] remoteExec ["BIS_fnc_holdActionRemove", 0, true];
        ["AF_DataDownload_startDownload", [_target]] call CBA_fnc_serverEvent;
    } else {
        titleText ["Odmowa dostępu! Nie wykryto penisa!", "PLAIN DOWN", 1];
    };
}, {
    // Code interrupted
    params ["_target", "_caller", "_actionId", "_arguments"];
    titleText ["Już koniec? Czego się boisz?", "PLAIN DOWN", 1];
}, [], 5, 10, false, false] remoteExec ["BIS_fnc_holdActionAdd", 0, _object];

["AF_DataDownload_AddHackAction", {
    params ["_object"];
    [_object, "Przerwij transfer danych", "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa", "_this distance _target < 3", "_caller distance _target < 3", {
        // Code start
        params ["_target", "_caller", "_actionId", "_arguments"];
        titleText ["Jesteś pewien? Od tego nie ma odwrotu.", "PLAIN DOWN", 1];
    }, {
        // Code progress
        params ["_target", "_caller", "_actionId", "_arguments", "_progress", "_maxProgress"];
    }, {
        // Code completed
        params ["_target", "_caller", "_actionId", "_arguments"];
        if (!isNil {_target getVariable ["AF_DataDownload_Success", nil]}) exitWith {
            [_target, _actionId] remoteExec ["BIS_fnc_holdActionRemove", 0, true];
        };
        private _msg = "";
        if (isPlayer _caller) then {
            _msg = format ["Dzban %1 przerwał pobieranie!", name _caller];
        } else {
            _msg = format ["%1 przerwał pobieranie!", name _caller];
        };
        [_msg] remoteExec ["systemChat", 0];
        ["AF_DataDownload_Hacked", [_target]] call CBA_fnc_serverEvent;
        [_target, _actionId] remoteExec ["BIS_fnc_holdActionRemove", 0, true];
    }, {
        // Code interrupted
        params ["_target", "_caller", "_actionId", "_arguments"];
        titleText ["Bardzo dobrze.", "PLAIN DOWN", 1];
    }, [], 5, 10, false, false] remoteExec ["BIS_fnc_holdActionAdd", 0, _object];
}] call CBA_fnc_addEventHandler;
