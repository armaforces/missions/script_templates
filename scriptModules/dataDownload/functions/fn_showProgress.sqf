/*
    AF_DataDownload_fnc_showProgress

    Description:
        Funkcja tworzy napis nad obiektem wskazujący postęp pobierania.

    Parameter(s):
        _object - obiekt nad którym ma się pokazywać postęp pobierania [OBJECT]

    Returns:
        NOTHING
*/

if (!hasInterface) exitWith {};

params ["_object"];

if (!isNil {_object getVariable ["AF_DataDownload_Success", nil]}) exitWith {};

private _fnc_draw = {
    // _this - obiekt nad którym ma być napis
    private _progressData = _this getVariable ["AF_DataDownload_ProgressData", [0, 0, 0]];
    _progressData params ["_progress", "_net", "_ETA"];
    ([_net, true] call AF_DataDownload_fnc_getUnit) params ["_netUnit", "_unit"];
    _ETA = [_ETA] call AF_DataDownload_fnc_formatTime;
    private _msg = format ["Data download progress: %1%2, Network usage: %3 %4/s, ETA: %5", _progress, "%", _netUnit, _unit, _ETA];
    drawIcon3D [
        "",
        [0.2, 0.5, 0.2, 0.9],
        _this modelToWorldVisual [0,0,.15],
        0,
        -2,
        0,
        _msg,
        2,
        0.03,
        "TahomaB",
        "center",
        true
    ];
};

private _fnc_remove = {
    params ["_object", "_pfhID"];
    private _downloadFinished = _object getVariable ["AF_DataDownload_success", nil];
    if (!isNil "_downloadFinished") exitWith {
        [_pfhID] call CBA_fnc_removePerFrameHandler;
    };
};

[{
    (_this#0) params ["_object", "_fnc_draw", "_fnc_remove"];
    if (player isEqualTo zeus) exitWith {
        [_object, (_this#1)] call _fnc_remove;
        _object call _fnc_draw;
    };
    if (player distance _object > 3) exitWith {
        [_object, (_this#1)] call _fnc_remove;
    };
    _intersect = lineIntersectsSurfaces [
        eyePos player,
        getPosASL _object,
        player,
        _object
    ];
    if (_intersect isEqualTo []) then {
        _object call _fnc_draw;
    };
    [_object, (_this#1)] call _fnc_remove;
}, 0, [_object, _fnc_draw, _fnc_remove]] call CBA_fnc_addPerFrameHandler;
