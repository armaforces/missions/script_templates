/*
    AF_DataDownload_fnc_downloadLoop

    Description:
        Funkcja zajmuje się procesem pobierania.

    Parameter(s):
        NOTHING

    Returns:
        NOTHING
*/

params ["_object"];

if (!alive _object || !isNil {_object getVariable ["AF_DataDownload_success", nil]}) exitWith {
    ["AF_DataDownload_failure"] call CBA_fnc_serverEvent;
};

private _net = call AF_DataDownload_Transfer;
private _dataDownloadProgress = _object getVariable ["AF_DataDownload_Progress", 0];
_dataDownloadProgress = _dataDownloadProgress + _net;
_object setVariable ["AF_DataDownload_Progress", _dataDownloadProgress];
private _progress = ((_dataDownloadProgress/AF_DataDownload_Target) * 100) toFixed 1;
([_net, true] call AF_DataDownload_fnc_getUnit) params ["_netUnit", "_unit"];
private _ETA = floor ((AF_DataDownload_Target - _dataDownloadProgress)/_net);
_object setVariable ["AF_DataDownload_ProgressData", [_progress, _net, _ETA], true];

if (_dataDownloadProgress >= AF_DataDownload_Target) exitWith {
    ["AF_DataDownload_success", [_object]] call CBA_fnc_serverEvent;
};

[AF_DataDownload_fnc_downloadLoop, [_object], 1] call CBA_fnc_waitAndExecute;
