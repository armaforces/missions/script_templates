/*
    AF_DataDownload_fnc_getUnit

    Description:
        Funkcja zwraca jednostkę (B/kB/MB/GB) dla danej wartości.

    Parameter(s):
        _value - wartość do przekonwertowania [NUMBER]
        _inBits - czy wynik ma być w bitach [BOOLEAN]

    Returns:
        0: wartość [NUMBER]
        1: jednostka [STRING]
*/

params ["_value", ["_inBits", false]];

_Units = [["B", "kB", "MB", "GB"], ["b", "kb", "Mb", "Gb"]] select _inBits;

private _newValue = _value;
private _divisions = 0;
while {_newValue >= 1024} do {
    _newValue = _newValue / 1024;
    _divisions = _divisions + 1;
};

[_newValue toFixed 2, _Units select _divisions];