class AF_DataDownload {
	class dataDownload {
			file = "scriptModules\dataDownload\functions";
			// Initialize dataDownload module
			class initModule {
				postInit = 1;
			};
			class assignActions {};
			class downloadLoop {};
			class formatTime {};
			class getUnit {};
			class showProgress {};
			class startDownload {};
	};
};